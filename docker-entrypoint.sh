#!/bin/sh
set -e

if [ -n "${TIMEZONE}" ]; then
  cp /usr/share/zoneinfo/${TIMEZONE} /etc/localtime
  echo "${TIMEZONE}" > /etc/timezone
  sed -i "s|^date.timezone =.*|date.timezone = ${TIMEZONE}|" /etc/php7/php.ini
fi

if [ -n "${XDEBUG_HOST}" ]; then
  sed -i "s|^;zend_extension|zend_extension|" /etc/php7/conf.d/xdebug.ini
  sed -i "s|^;xdebug.remote_enable|xdebug.remote_enable|" /etc/php7/conf.d/xdebug.ini
  export XDEBUG_CONFIG="remote_host=${XDEBUG_HOST}"
fi

exec "$@"
