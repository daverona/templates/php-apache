FROM alpine:3.10

# Setting the system timezone
# @see https://wiki.alpinelinux.org/wiki/Setting_the_timezone
ARG TIMEZONE=UTC
RUN apk add --no-cache tzdata \
  && cp /usr/share/zoneinfo/$TIMEZONE /etc/localtime \
  && echo "$TIMEZONE" > /etc/timezone

# Installing Apache2, redis and supervisor
RUN apk add --no-cache apache2 apache2-ssl apache2-utils git \
    mariadb-client python3 redis \
  && pip3 install --no-cache-dir supervisor \
  && mkdir -p /var/log/supervisor
COPY ./supervisor/supervisord.conf /etc/
COPY ./supervisor/supervisor.ini /etc/supervisor.d/

# Securing Apache2
# @see https://www.tecmint.com/apache-security-tips
# @see https://wpbuffs.com/apache-security-best-practices/
RUN sed -i -e "s|^ServerTokens OS|ServerTokens Prod|" \
    -e "s|^ServerSignature On|ServerSignature Off|" \
    /etc/apache2/httpd.conf \
  && echo "FileETag None" >> /etc/apache2/httpd.conf

# Installing PHP 7, composer and extensions
RUN apk add --no-cache composer php7-apache2 php7-ctype php7-curl php7-dom \
    php7-gd php7-intl php7-json php7-mbstring php7-mcrypt php7-mysqli \
    php7-openssl php7-pdo php7-phar php7-redis php7-tokenizer php7-xml \
    php7-xmlreader php7-xmlwriter php7-zip php7-zlib \
  && sed -i "s|^;date.timezone =.*|date.timezone = ${TIMEZONE}|" \
    /etc/php7/php.ini

# Installing and customizing PHP Xdebug extension
RUN apk add --no-cache php7-xdebug \
  && sed -i "s|^zend_extension|;zend_extension|" /etc/php7/conf.d/xdebug.ini \
  && echo ";xdebug.remote_enable=on" >> /etc/php7/conf.d/xdebug.ini

# Customizing Apache2 for Laravel
# RUN sed -i "s|/var/www/localhost/htdocs|/var/www/localhost/htdocs/public|" /etc/apache2/httpd.conf

# Configuring entrypoint, ports and working directory
COPY ./docker-entrypoint.sh /
RUN chmod +x /docker-entrypoint.sh
EXPOSE 80/tcp 443/tcp
WORKDIR /var/www/localhost/htdocs

ENTRYPOINT ["/docker-entrypoint.sh"]
CMD ["supervisord", "-c", "/etc/supervisord.conf", "-n"]
