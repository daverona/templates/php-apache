# daverona/php-apache

[![pipeline status](https://gitlab.com/toscana/docker/php-nginx/badges/master/pipeline.svg)](https://gitlab.com/toscana/docker/php-nginx/commits/master)

* GitLab source repository: [https://gitlab.com/toscana/docker/php-apache](https://gitlab.com/toscana/docker/php-apache)
* Docker Hub repository: [https://hub.docker.com/r/daverona/php-apache](https://hub.docker.com/r/daverona/php-apache)

This is a Docker image of Apache, PHP and Redis stack on Alpine Linux. The image provides:

* [PHP](https://www.php.net/) 7.3.6
* [Apache](https://httpd.apache.org/docs/2.4/) 2.4.39
* [Redis](https://redis.io/documentation) 5.0.5
* [Alpine Linux](https://alpinelinux.org/) 3.10

## Installation

Install [Docker](https://hub.docker.com/search/?type=edition&offering=community)
if you don't have one.  Then pull the image from Docker Hub repository:

```bash
docker image pull daverona/php-apache
```

or build the image:

```bash
docker image build \
  --build-arg TIMEZONE=UTC \
  --tag daverona/php-apache \
  .
```

When you build the image, you can specify a time zone for Alpine Linux and PHP `date.timezone` in PHP.ini as shown above. If you don't specify the value of build argument `TIMEZONE`, UTC will be used.

## Quick Start

Run the container:

```bash
docker container run --rm \
  --detach \
  --publish 80:80 \
  --publish 443:443 \
  --name app \
  daverona/php-apache
```

Visit [http://localhost](http://localhost) and [https://localhost](https://localhost) and verify that Apache serves HTTP and HTTPS.

## Development

### Persistence

Run the container with your PHP source directory mounted:

```bash
docker container run --rm \
  --detach \
  --publish 80:80 \
  --publish 443:443 \
  --volume /host/path/to/src:/var/www/localhost/htdocs \
  --name app \
  daverona/php-apache
```

In the above example directory /host/path/to/src is
where your PHP source resides and you must change it properly.

### PHP Xdebug extension

If you want to debug using PHP Xdebug extension:

```bash
docker container run --rm \
  --detach \
  --publish 80:80 \
  --publish 443:443 \
  --volume /host/path/to/src:/var/www/localhost/htdocs \
  --env XDEBUG_HOST=192.168.0.1 \
  --name app \
  daverona/php-apache
```

You must change 192.168.0.1 to your IP address in the example. Xdebug extension will be enabled if and only if the value of environment variable `XDEBUG_HOST` is given to the container.
You should *not* use localhost or 127.0.0.1 (i.e. loopback) for `XDEBUG_HOST` because for the container it is the address of the container, not your host.

### Redis

Redis is built-in. The Redis server address is the container's loopback and the port is 6379, which is default.

## Customization

### SSL Certificate

The image has a self-signed SSL certificate and a private key used to sign the certificate. If you like to replace them:

```bash
docker container run --rm \
  ...
  --volume /host/path/to/certificate:/etc/ssl/apache2 \
  ...
  daverona/php-apache
```

where /host/path/to/certificate is a host directory that contains your SSL certificate file `server.pem` and private key file `server.key`.

### Laravel

For [Laravel](https://laravel.com/docs/5.8), the document root should point to Laravel's `public` folder. Find the following line in Dockerfile and uncomment it:

```docker
RUN sed -i "s|/var/www/localhost/htdocs|/var/www/localhost/htdocs/public|" /etc/apache2/httpd.conf
```

Then build the image again.

## Production considerations

### PHP Xdebug extension

Although it is disabled by default, Xdebug extension is better not included in the image for performance and security reasons. To remove it from the image, find and comment out the following block in Dockerfile:

```docker
# Installing and customizing PHP Xdebug extension
RUN apk add --no-cache php7-xdebug \
  && sed -i "s|^zend_extension|;zend_extension|" /etc/php7/conf.d/xdebug.ini \
  && echo ";xdebug.remote_enable=on" >> /etc/php7/conf.d/xdebug.ini
```

Also find and comment out the following block in docker-entrypoint.sh:

```sh
if [ -n "${XDEBUG_HOST}" ]; then
  sed -i "s|^;zend_extension|zend_extension|" /etc/php7/conf.d/xdebug.ini
  sed -i "s|^;xdebug.remote_enable|xdebug.remote_enable|" /etc/php7/conf.d/xdebug.ini
  export XDEBUG_CONFIG="remote_host=${XDEBUG_HOST}"
fi
```

Then build the image again.

## References

* [https://www.tecmint.com/apache-security-tips/](https://www.tecmint.com/apache-security-tips/)
